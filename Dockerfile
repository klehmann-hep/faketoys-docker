# Run locally with
#   docker build -t kolehman/faketoys:base --target base .
#   docker build -t kolehman/faketoys:base-documentation --target base-documentation .

ARG PYTHON_VERSION=3.7-slim

# base image with all necessary dependencies
FROM python:$PYTHON_VERSION AS base
RUN groupadd -r -g 1000 docker && useradd --no-log-init -r -g docker -u 1000 docker
RUN pip install \
        "coverage==5.1" \
        "dask[array]==2.18.1" \
        "dask[dataframe]==2.18.1" \
        "dask==2.18.1" \
        "matplotlib==3.2.1" \
        "numpy==1.18.2" \
        "scipy==1.4.1" \
        "uncertainties==3.1.2" \
        "xarray==0.16.1" \
        --no-cache-dir
RUN apt-get -qq -y update && \
    apt-get -qq -y install --no-install-recommends git && \
    apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt-get/lists/*
CMD /bin/bash

# add sphinx for documentation
FROM base as base-documentation
RUN pip install \
        "sphinx==3.3.1" \
        "sphinx-rtd-theme==0.5.0" \
        --no-cache-dir
RUN apt-get -qq -y install --no-install-recommends make && \
    apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt-get/lists/*
