Faketoys docker
---
This repository builds the base image with all dependencies for the [faketoys](https://gitlab.com/klehmann-hep/faketoys) project. It creates two images and pushes them to [docker hub](https://hub.docker.com/repository/docker/kolehman/faketoys): `base` and `base-documentation`. The `base` image contains all necessary runtime dependencies, the `base-documentation` image also contains the packages needed for documentation.

Any tags created in this repository should coincide with tags in [faketoys](https://gitlab.com/klehmann-hep/faketoys).
